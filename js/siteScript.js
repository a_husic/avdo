function setActive(id){
    disableActive();
    jQuery("#"+id).addClass("activeLink");
}
function disableActive(){
    jQuery("#menu-li-title").removeClass("activeLink");
    jQuery("#menu-li-product").removeClass("activeLink");
    jQuery("#menu-li-news").removeClass("activeLink");
    jQuery("#menu-li-about").removeClass("activeLink");
    jQuery("#menu-li-contact").removeClass("activeLink");
}